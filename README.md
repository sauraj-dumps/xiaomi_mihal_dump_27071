## missi_phone_eea-user 14 UP1A.230905.011 V816.0.5.0.ULQEUXM release-keys
- Manufacturer: xiaomi
- Platform: common
- Codename: mihal
- Brand: Xiaomi
- Flavor: missi_phone_eea-user
- Release Version: 14
- Kernel Version: 5.10.168
- Id: UP1A.230905.011
- Incremental: V816.0.5.0.ULQEUXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Xiaomi/mihal/mihal:12/SP1A.210812.016/V816.0.5.0.ULQEUXM:user/release-keys
- OTA version: 
- Branch: missi_phone_eea-user-14-UP1A.230905.011-V816.0.5.0.ULQEUXM-release-keys
- Repo: xiaomi_mihal_dump_27071
